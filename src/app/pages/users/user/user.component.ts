import { UserService } from '../../../services/user/user.service';
import { LoanService } from '../../../services/loan/loan.service';
import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { User } from '../../../models/user.interface';
import { Loan } from '../../../models/loan.interface';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { NotificationService } from '../../../services/notification/notification.service';
import { MatDialog } from "@angular/material/dialog";
import { TranslateService } from "@ngx-translate/core";
import { ModalComponent } from '../../../utils/modal/modal.component';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent implements OnInit {
  public userData: any;
  public loans$: Observable<Loan[]>;
  userId: string;
  loans: any;
  displayedColumns: string[] = ['name', 'value', 'status', 'actions',];

  constructor(
    private router: ActivatedRoute,
    private userService: UserService,
    private loanService: LoanService,
    private translate: TranslateService,
    public dialog: MatDialog,
    private _notification: NotificationService
  ) { }

  ngOnInit(): void {
    this.userId = this.router.snapshot.params.id;
    this.getUser(this.userId);
    this.getLoans(this.userId);
  }

  getUser(id: string) {
    this.userService.getUserInfo(id).subscribe(data => {
      this.userData = data;
    });
  }

  getLoans(id: string) {
    this.loans$ = this.loanService.getLoansByUserId(id);
  }

  payLoan(data) {
    data.paymentStatus = true;
    data.paymentDate = new Date();
    this.loanService.updateLoan(data).then((user) => {
      this.translate.get("SUCESSFUL_PAYMENT").subscribe((res: string) => {
        this._notification.showSuccessMessage(res);
      });
    })
  }

  applyNewLoan() {
    const editDialog = this.dialog.open(ModalComponent);
    editDialog.afterClosed().subscribe((result) => {
      if (result) {
        this.newLoan({ loanValue: result });
      }
    });
  }

  newLoan(loan) {
    loan.creditStatus = this.randomBoolean();
    loan.paymentStatus = false;
    this.userData.lastCreditApproved = loan.creditStatus;
    this.userData.id = this.userId;
    this.userService.updateUser(this.userData).then((userRes) => {
      loan.userId = this.userData.id;
      this.loanService.saveLoan(loan).then((loanRes) => {
        if (loan.creditStatus) {
          this.translate.get("LOAN_APPROVED").subscribe((res: string) => {
            this._notification.showSuccessMessage(res);
          });
        }
        else {
          this.translate.get("LOAN_DENIED").subscribe((res: string) => {
            this._notification.showSuccessMessage(res);
          });
        }
      });
    });
  }



  randomBoolean() {
    return Boolean(Math.round(Math.random()));
  }




}
