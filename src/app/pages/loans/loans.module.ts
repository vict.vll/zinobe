import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';

// TRANSLATE
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

// MATERIAL COMPONENTS
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';

import { LoansRoutingModule } from './loans-routing.module';
import { LoansComponent } from './loans.component';
import { Pipes } from '../../pipes/pipes.module';
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, '/assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    LoansComponent
  ],
  imports: [
    CommonModule,
    LoansRoutingModule,
    TranslateModule.forRoot({
      defaultLanguage: 'es',
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient],
      },
    }),
    MatButtonModule,
    MatIconModule,
    MatButtonModule,
    MatSortModule,
    MatTableModule,
    MatChipsModule,
    Pipes
  ]
})
export class LoansModule { }
