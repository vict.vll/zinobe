import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ApplyForLoansComponent } from './apply-for-loans.component';

const routes: Routes = [{ path: '', component: ApplyForLoansComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApplyForLoansRoutingModule { }
