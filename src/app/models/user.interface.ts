export interface User {
  firstName: string;
  lastName: string;
  email: string;
  documentId: string;
  moneyRequest: string;
  paymentDate: string;
  creditStatus: boolean;
  paymentStatus: boolean;
  loanValue: number;
  lastCreditApproved: boolean;
  id?: string;
}
