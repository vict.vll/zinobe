export interface Capital {
    capital: number;
    loans: number;
    payments: number;
    total: number;
    id?: string;
}
