import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../../models/user.interface';
import { CapitalService } from '../capital/capital.service'
@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private afs: AngularFirestore, private capital: CapitalService) { }

  public getAllUsers(): Observable<User[]> {
    return this.afs
      .collection('users')
      .snapshotChanges()
      .pipe(
        map((actions) =>
          actions.map((a) => {
            const data = a.payload.doc.data() as User;
            const id = a.payload.doc.id;
            return { id, ...data };
          })
        )
      );
  }

  public getUserInfo(id: string): Observable<User> {
    return this.afs.doc<User>(`users/${id}`).valueChanges();
  }

  public saveUser(data: User) {
    return this.afs.collection('users').add(data);
  }

  public updateUser(data: User) {
    return this.afs.collection('users').doc(data.id).update(data);
  }

}
