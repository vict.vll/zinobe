import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { SnackbarComponent } from "../../utils/snackbar/snackbar.component";

@Injectable({
  providedIn: "root",
})
export class NotificationService {
  constructor(
    private _snackBar: MatSnackBar
  ) { }
  showSuccessMessage(message) {
    this._snackBar.openFromComponent(SnackbarComponent, {
      data: message,
      duration: 2000,
      horizontalPosition: "end",
      verticalPosition: "top",
    });
  }
}
