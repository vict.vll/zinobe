import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../../models/user.interface';
import { Loan } from '../../models/loan.interface';
import { CapitalService } from '../capital/capital.service';

@Injectable({
  providedIn: 'root'
})
export class LoanService {
  constructor(private afs: AngularFirestore, private capital: CapitalService) { }

  public getAllLoans(): Observable<Loan[]> {
    return this.afs
      .collection('loans')
      .snapshotChanges()
      .pipe(
        map((actions) =>
          actions.map((a) => {
            const data = a.payload.doc.data() as Loan;
            const id = a.payload.doc.id;
            return { id, ...data };
          })
        )
      );
  }

  public saveLoan(data: Loan) {
    if (data.creditStatus) {
      this.capital.updateCapital(data.loanValue, 1);
    }
    return this.afs.collection('loans').add(data);
  }

  public getLoansByUserId(id: string): Observable<Loan[]> {
    return this.afs.collection('loans', ref => ref.where('userId', '==', id))
      .snapshotChanges()
      .pipe(
        map((actions) =>
          actions.map((a) => {
            const data = a.payload.doc.data() as Loan;
            const id = a.payload.doc.id;
            return { id, ...data };
          })
        )
      );
  }

  public updateLoan(data: Loan) {
    this.capital.updateCapital(data.loanValue, 2);
    return this.afs.collection('loans').doc(data.id).update(data);
  }

}
