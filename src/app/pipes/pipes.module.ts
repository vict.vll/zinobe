import { NgModule } from '@angular/core';
import { TimestampPipe } from './timestamp.pipe';

@NgModule({
    declarations: [
        TimestampPipe,
    ],
    imports: [],
    exports: [
        TimestampPipe,
    ]
})
export class Pipes { }
