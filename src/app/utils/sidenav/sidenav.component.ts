import { Component } from '@angular/core';
import { Constants } from '../../../config/constants';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
})
export class SidenavComponent {
  sidenavOptions = Constants.SIDENAV_OPTIONS;
  constructor() {}
}
