import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  sidenavOpen: boolean = false;
  @Output() sidenavStatus = new EventEmitter<string>();

  constructor() { }

  sidebarToggle() {
    this.sidenavStatus.emit();
  }
}
