// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyAArP-_Wpldoo5ltNp4j_iydKjBs1Lvh4k',
    authDomain: 'zinobe-test-5f7d4.firebaseapp.com',
    databaseURL: 'https://zinobe-test-5f7d4.firebaseio.com',
    projectId: 'zinobe-test-5f7d4',
    storageBucket: 'zinobe-test-5f7d4.appspot.com',
    messagingSenderId: '487092137903',
    appId: '1:487092137903:web:6a902c577ae545f1e3895a',
  },
  capitalBank: 1000000,
  loanValue: 100000,
  minLoanValue: 10000,
  amountFunctions: 10000,
  capitalId: '8sSmhYldHuo7lqVhNpZt'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
